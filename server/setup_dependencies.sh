#!/usr/bin/env bash

cd /var/www/html/

composer install
# npm -g config set user root
npm install
npm run build
