#!/bin/bash

if [ $(systemctl list-unit-files | grep docker-easyappointments | wc -l) -eq 1 ]
then
    sudo systemctl stop docker-easyappointments
else
    docker-compose stop
fi
