#!/bin/bash

BASE_DIR=$(dirname "$0")

# Ask for confirmation unless -f parameter is provided
if [ $# -eq 0 ]
then
    while true; do
	read -p "This will overwrite existing data of easyappointments-database container? Are you sure? (Y/N)" yn
	case $yn in
            [Yy]* ) break;;
            [Nn]* ) echo "Aborting"; exit 0;;
            * ) echo "Please answer Y (yes) or N (no).";;
	esac
    done
elif [ "$1" = "-f" ]
then
    echo "Restoration forced"
else
    echo "Unknown parameter $1"
    exit 1
fi

# Test if backup exist
if [ ! -d "${BASE_DIR}/backup" ]
then
    echo "backup directory not found. Exit"
    exit 1
fi
if [ ! -f "${BASE_DIR}/backup/easyappointments_db.tar.gz" ]
then
    echo "Database backup file easyappointments_db.tar.gz not found in backup directory. Exit"
    exit 1
fi

# Test if container exists
if [ ! $(docker container ls --all --format="{{.Names}}" | grep "^easyappointments-database$" | wc -l) -eq 1 ]
then
    echo -e "Can not find container easyappointments-database.\nPerhaps you should setup the system (setup.sh) before."
    exit 1
fi

# Test if container is started
CONT_STARTED=0
if [ $(docker container ls --format="{{.Names}}" | grep "^easyappointments-.*$" | wc -l) -gt 0 ]
then
    CONT_STARTED=1
fi


# Performing restoration
cd ${BASE_DIR}

# Stop running container
if [ $CONT_STARTED -eq 1 ]
then
    echo "Stopping containers"
    ./stop.sh > /dev/null
    # Wait until container is stopped
    docker container wait easyappointments-database > /dev/null
fi

echo "Restoring database"
docker container run \
       --rm  \
       --volumes-from easyappointments-database \
       -v $(pwd)/backup:/tmp \
       debian:latest \
       bash -c "cd /var/lib/mysql && rm -Rf ./* && tar xzf /tmp/easyappointments_db.tar.gz --strip 3"

# Start container (if it was running before restoration)
if [ $CONT_STARTED -eq 1 ]
then
    echo "Starting containers"
    ./start.sh > /dev/null
fi

