#!/bin/bash

BASE_DIR=$(dirname "$0")

# Create backup directory
if [ ! -d "${BASE_DIR}/backup" ]
then
    mkdir ${BASE_DIR}/backup
fi

cd ${BASE_DIR} || exit 1

# Test if container is started
CONT_STARTED=0
if [ $(docker container ls --format="{{.Names}}" | grep "^easyappointments-.*$" | wc -l) -gt 0 ]
then
    CONT_STARTED=1
fi

# Stop running container
if [ $CONT_STARTED -eq 1 ]
then
    echo "Stopping containers"
    ./stop.sh > /dev/null
    # Wait until container is stopped
    docker container wait easyappointments-database > /dev/null
fi

echo "Backup database"
docker container run \
       --rm  \
       --volumes-from easyappointments-database \
       -v $(pwd)/backup:/tmp \
       debian:latest \
       tar czf /tmp/easyappointments_db_$(date +%Y-%m-%d_%H-%M-%S).tar.gz -C / var/lib/mysql

# Start container (if it was running before backup)
if [ $CONT_STARTED -eq 1 ]
then
    echo "Starting containers"
    ./start.sh > /dev/null
fi
