#!/bin/bash

source env.sh

# Pause to edit configuration
while true; do
    read -p 'Edit configuration files (docker-compose.yml, server.env, db.env, server/mstmprc, easyappointments_config.php) then press enter' yn
    case $yn in
        * ) break;;
    esac
done

# Confirm replacement of existing sources
if [ -d easyappointments ]
then
    read -p 'This script will replace existing easyappointements directory. Do you want to proceed? (Y/N)' yn
    case $yn in
        [Yy]* ) rm -Rf easyappointments;;
        * ) echo 'Aborting'; exit 0;;
    esac
fi

if [ ! -f easyappointments.zip ]
then
    curl -L -o easyappointments.zip ${REPO}/releases/download/${VERSION}/easyappointments-${VERSION}.zip
fi
unzip easyappointments.zip -d easyappointments

# Set EA configuration file
cp easyappointments_config.php easyappointments/config.php
source db.env
sed -i -e 's|_DB_NAME_|'${MYSQL_DATABASE}'|' easyappointments/config.php
sed -i -e 's|_DB_USERNAME_|'${MYSQL_USER}'|' easyappointments/config.php
sed -i -e 's|_DB_PASSWORD_|'${MYSQL_PASSWORD}'|' easyappointments/config.php

# Files' permissions
echo 'Set PHP code files permissions'
sudo chgrp -R www-data easyappointments/
find easyappointments/ -type f -exec chmod 640 {} \;
find easyappointments/ -type d -exec chmod 750 {} \;
for d in storage # assets vendor
do
    find easyappointments/$d -type f -exec chmod 660 {} \;
    find easyappointments/$d -type d -exec chmod 770 {} \;
done


# System logging
# if [ ! -d ./log ]
# then
# 	mkdir -p log
# fi
# if [ $(stat -c '%U:%G' log/) != "root:www-data" ]
# then
# 	sudo chown -R root:www-data log/
#     sudo chmod 770 log/
# fi

# Building images & containers
docker-compose up --build --no-start --force-recreate
echo 'Containers created'

read -p 'Do you want to set systemd service? (Y/N)' yn
case $yn in
    [Yy]* )
        cp docker-easyappointments-sample.service docker-easyappointments.service
        sed -i -e 's|_USER_|'$(whoami)'|' docker-easyappointments.service
        sed -i -e 's|_PATH_EA_PROJECT_|'$(pwd)'|' docker-easyappointments.service
        sudo cp docker-easyappointments.service /etc/systemd/system/
        sudo chown root:root /etc/systemd/system/docker-easyappointments.service
        sudo systemctl daemon-reload
        echo 'docker-easyappointments.service created. You can manage it with systemd (enable, start, stop...)'
esac

echo 'Done'
exit 0
