#!/usr/bin/env bash

source env.sh

docker-compose up --no-build --no-start --no-recreate
