#!/bin/bash

while true; do
    read -p "This will definitively delete all containers of Easyappointments and their volumes (data)? Are you sure? (Y/N)" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) echo "Aborting"; exit 0;;
        * ) echo "Please answer Y (yes) or N (no).";;
    esac
done

docker-compose rm -v -f
docker volume rm easyappointments_db > /dev/null
