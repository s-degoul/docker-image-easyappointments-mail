#!/bin/bash

if [ $(systemctl list-unit-files | grep docker-easyappointments | wc -l) -eq 1 ]
then
    sudo systemctl start docker-easyappointments
else
    docker-compose start
fi
